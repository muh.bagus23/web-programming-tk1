<?php

use App\Http\Controllers\VideoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group(function(){
    Route::get('/my-videos', [VideoController::class, 'myVideo'])->name('myVideo');
    
    Route::prefix('video')->name('video.')->group(function(){
        Route::get('/create', [VideoController::class, 'create'])->name('create');
        Route::post('/create', [VideoController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [VideoController::class, 'edit'])->name('edit');
        Route::put('/update', [VideoController::class, 'update'])->name('update');
        Route::delete('/delete', [VideoController::class, 'delete'])->name('delete');
    });
});