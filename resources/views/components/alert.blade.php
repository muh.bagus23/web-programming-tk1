@if(session('msg'))
    <div class="col-12 alert alert-success">{{ session('msg') }}</div>
@endif

@if(session('error'))
    <div class="col-12 alert alert-danger">{{ session('error') }}</div>
@endif