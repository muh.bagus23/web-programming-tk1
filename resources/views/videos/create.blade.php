@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="POST" action="{{ route('video.store') }}" enctype="multipart/form-data">
            @csrf
            @include('components.alert')
            @if($errors->any())
                {!! implode('', $errors->all('<div class="col-12 alert alert-danger">:message</div>')) !!}
            @endif
            <div class="card">
                <div class="card-header">{{ __('Upload Your Video') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-7">
                            <div class="mb-3">
                                <div class="mb-2">Title</div>
                                <input type="text" class="form-control" name="title" value="{{ $video->title ?? old('title') }}" />
                            </div>
                            <div>
                                <div class="mb-2">Video</div>
                                <input type="file" id="video-file" class="form-control" name="video_file" />
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="thumbnail thumbnail-image-placeholder">
                                Preview
                            </div>
                            <video class="col-12 d-none" id="video-element" controls>
                                <source type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="col-12 text-end">
                        <button type="submit" class="btn btn-success">Upload Now!</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    })
    
    document.querySelector("#video-file").addEventListener('change', function() {
        document.querySelector('#video-element').classList.remove('d-none')
        document.querySelector('.thumbnail-image-placeholder').classList.add('d-none')

        toBase64(document.querySelector("#video-file").files[0]).then(function(result){
            document.querySelector("#video-element source").setAttribute('src', result);
            document.querySelector("#video-element").load()
        })
    });
</script>
@endsection
