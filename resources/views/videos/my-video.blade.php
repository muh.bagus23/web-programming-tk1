@extends('layouts.app')

@section('style')
<style>
    .video-player video{
        width: 100% !important;
    }

    .video-player .description small{
        color: #777;
    }

    .video-player .description .title{
        font-size: 18px;
    }
</style>
<link href="https://vjs.zencdn.net/7.20.3/video-js.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            @include('components.alert')

            @if(count($videos))
            <div class="row">
                @foreach($videos as $video)
                <div class="col-12 col-lg-4 mb-4 video-player">
                    <div class="card">
                        <video style="width: 100% !important;" class="video-js" controls preload="auto" data-setup="{}">
                            <source src="{{ $video->video_url }}" type="video/mp4" />
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a
                                web browser that
                                <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                            </p>
                        </video>
                        <div class="description p-2">
                            <small>{{ \Carbon\Carbon::parse($video->date)->format('j F Y H:i') }}</small>
                            <div class="title">{{ $video->title }}</div>
                        </div>
                        <div class="card-footer p-2 d-flex justify-content-end">
                            <a class="btn btn-danger me-auto" href="#" onclick="return (confirm('Are you sure want to delete this video?') ? document.querySelector('form#delete-{{ $video->id }}').submit() : false)">Delete</a>
                            <form action="{{ route('video.delete') }}" id="delete-{{ $video->id }}" method="POST">
                                @method('delete')
                                @csrf
                                <input hidden name="id" value="{{ $video->id }}" />
                            </form>

                            <a class="btn btn-warning" href="{{ route('video.edit', $video->id) }}">Edit</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://vjs.zencdn.net/7.20.3/video.min.js"></script>
@endsection
