<?php

namespace App\Libs;

use Illuminate\Support\Facades\File;

class UploaderLib
{
    public static function upload($file)
    {
        $uploadPath = '/uploads/video';
        $targetUpload = public_path().$uploadPath;
        if (!File::exists($targetUpload)) {
            File::makeDirectory($targetUpload, 0777, true, true);
        }

        $filename = implode('.', [strtotime(date('Y-m-d H:i:s')).'-'.rand(1, 9999), $file->extension()]);
		$file->move($targetUpload, $filename);

        return asset($uploadPath.'/'.$filename);
    }
}
