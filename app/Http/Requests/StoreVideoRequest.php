<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'video_file' => ['required', 'file', 'mimetypes:video/avi,video/mp4,video/mpeg,video/quicktime']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'title.string' => 'Title must be type of string',
            'video_file.required' => 'Please video to upload',
            'video_file.file' => 'Video must be type of file',
            'video_file.mimetypes' => 'Please upload video with these extension avi, mp4, mpeg, quicktime'
        ];
    }
}
