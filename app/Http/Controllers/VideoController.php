<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;
use App\Libs\UploaderLib;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    public function myVideo()
    {
        $videos = Video::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->paginate(10);
        return view('videos.my-video', [
            'videos' => $videos
        ]);
    }

    public function create()
    {
        return view('videos.create');
    }

    public function store(StoreVideoRequest $request)
    {
        DB::beginTransaction();
        try {
            $file = UploaderLib::upload($request->file('video_file'));

            Video::create([
                'title' => $request->title,
                'video_url' => $file,
                'user_id' => auth()->user()->id,
                'date' => Carbon::now()
            ]);

            DB::commit();

            return redirect()->route('myVideo')->with('msg', 'Video uploaded successfully!');
        } catch (\Throwable $th) {
            DB::rollBack();
            
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }

    public function edit($id)
    {
        $video = Video::findOrFail($id);
        return view('videos.edit', [
            'video' => $video
        ]);
    }

    public function update(UpdateVideoRequest $request)
    {
        DB::beginTransaction();
        try {
            $video = Video::find($request->id);
            if (!$video) {
                return redirect()->back()->withInput()->with('error', 'Video not found');
            }

            $file = $video->video_url;
            if ($request->file('video_file')) {
                $file = UploaderLib::upload($request->file('video_file'));
            }

            Video::where('id', $request->id)->update([
                'title' => $request->title,
                'video_url' => $file,
                'user_id' => auth()->user()->id,
                'date' => Carbon::now()
            ]);

            DB::commit();

            return redirect()->route('myVideo')->with('msg', 'Video uploaded successfully!');
        } catch (\Throwable $th) {
            DB::rollBack();
            
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {
            Video::where('id', $request->id)->delete();

            DB::commit();

            return redirect()->route('myVideo')->with('msg', 'Video deleted successfully!');
        } catch (\Throwable $th) {
            DB::rollBack();
            
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }
}
